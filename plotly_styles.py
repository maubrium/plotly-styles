"""A collection of plotly layouts, color themes, and a few helper functions
 for making routine plots and tables.

Plotly Layouts (set within go.Figure()):
    layout: The group standard single column figure.
    responsive_layout: Take the full width of the screen. Set layout.height = None to take up full height as well.
Colors:
    Colorways are defined as lists of colors.
    colorway: A clear, vibrant color pallet that is easily distinguishable on all screens
    austin: The UT Austin color pallet based on shades of the two of the grey and orange.
    gruv: Based on the Gruvbox color scheme. These colors look great at all scales.
        Limited testing in print or on old projectors.
        Lists available: gruv, gruv-light and gruv-dark, gruv_mono
    nord: (Rare usage) A classy color pallet built on shades of blue and grey. Best used for back lit screens only.
        these are largely acceptable colors for gradients and analagous data sets
Color Functions:
    gradient: takes a list of colors and a number of steps and generates a list of colors on the gradient
    display_colors: Takes a list of colors and shows an image of them.
    hex2rgb: convert hex string to rgb values
    rgb2hex: convert rgb values to hex string

"""
import json
import os

from PIL import Image
from collections import OrderedDict
import IPython.display
import plotly.graph_objs as go

###################################################
###################   Layouts   ###################
###################################################
'''Defines our standard plot layout'''
__location__ = os.path.realpath(os.path.join(
    os.getcwd(), os.path.dirname(__file__)))
with open(os.path.join(__location__, 'layout.json')) as layout_f:
    layout_data = layout_f.read()
    layout = json.loads(layout_data)

layout = dict(
    layout = layout
)
layout=go.layout.Template(layout)
layout.layout.xaxis.showline=True
layout.layout.yaxis.showline=True
layout.layout.autosize=False
layout.layout.width=780
layout.layout.height=510



config= dict(
    modeBarButtonsToAdd=['toggleSpikelines'], 
    displaylogo=False, 
    toImageButtonOptions= dict(
                        format='svg', 
                        filename='fig'
                     )
            )
custom_config= dict(
    modeBarButtonsToAdd=['toggleSpikelines'], 
    displaylogo=False, 
    toImageButtonOptions= dict(
                        format='svg', 
                        filename='fig'
                     )
            )

'''Defines a responsive layout useful or data exploration'''
with open(os.path.join(__location__, 'responsive_layout.json')) as responsive_layout_f:
    responsive_layout_data = responsive_layout_f.read()
    responsive_layout = json.loads(responsive_layout_data)


# UPDATED to be identical to "layout" see above.
'''Defines a 3 inch figure to Long Group Standards for two column publication'''
with open(os.path.join(__location__, 'layout_LG3.json')) as layout_LG3_f:
    layout_LG3_data = layout_LG3_f.read()
    layout_LG3 = json.loads(layout_LG3_data)

'''Defines basic table layouts from excel imports'''
with open(os.path.join(__location__, 'layout_LG3.json')) as table_layout__f:
    table_layout_data = table_layout__f.read()
    table_layout = json.loads(table_layout_data)

# update config to remove plotly link
config = {'showLink': False, 'displaylogo': False}

###################################################
####################   Colors   ####################
###################################################

# The standard colorset
colorway = ["#4286f4", "#95c631", "#F49007",
            "#ffd600", "#ff5959", "#ca7cdd",
            "#323232", "#a0c2f9", "#cae298",
            "#f9c783", "#ffacac", "#e4bdee",
            "#989898"]

colors = {
    'blue': "#4286f4",
    'green': "#95c631",
    'orange': "#F49007",
    'yellow': "#ffd600",
    'red':  "#ff5959",
    'purple': "#ca7cdd",
    'grey': "#323232",
    'light-blue': "#a0c2f9",
    'light-green': "#cae298",
    'light-orange': "#f9c783",
    'light-red': "#ffacac",
    'light-purple': "#e4bdee",
    'light-grey': "#989898",
    'white': '#FFFFFF',
    'black': '#000000'
}

# Named colors of colorway
ut_colors = {'blue': '#005f86',
             'green': '#579D42',
             'yellow': '#FFD600',
             'orange': '#BF5700',
             'red': '#DB4D4D',
             'grey': '#333F48',
             'cyan': '#00a9b7',
             'sand': '#d6d2c4',
             'light-grey': '#9cadb7',
             'light-green': '#a6cd57',
             'light-orange': '#f8971f',
             'pale-orange': '#c38a5b',
             'pale-grey': '#eaebea'
             }

# Custom colorways
nord = ['#2E3440', '#3B4252', '#3B4252', '#3B4252', '#D8DEE9', '#E5E9F0', '#ECEFF4', '#8FBCBB',
        '#88C0D0', '#81A1C1', '#5E81AC', '#BF616A', '#D08770', '#EBCB8B', '#A3BE8C', '#B48EAD']

austin = ['#333F48', '#BF5700', '#005F86', '#579D42', '#FFD600',
          '#F8971F', '#00A9B7', '#A6CD57', '#D6D2C4']


def rgb2hex(triplet):
    """Convert rgb list to hexcode

    Arguments:
        triplet {list} -- list of rbg values each between 0 and 255

    Returns:
        str -- hex code string with leading #
    """
    hex = "#{:02x}{:02x}{:02x}".format(triplet[0], triplet[1], triplet[2])
    return hex


def hex2rgb(string):
    """Convert hex code string to rgb

    Arguments:
        string {str} -- [description]

    Returns:
        [list] -- list of rgb values
    """
    h = string.strip('#')
    return tuple(int(h[i:i+2], 16) for i in (0, 2, 4))

# for 2 colors only


def bigradient(start, stop, n):
    """Takes in two colors as hex codes and returns a gradient with n steps

    Arguments:
        start {str} -- hex code for first color
        stop {str} -- hex code for last color
        n {int} -- number of colors in the final output list

    Returns:
        list -- list of hex codes
    """
    dist = [hex2rgb(stop)[i]-hex2rgb(start)[i] for i in range(0, 3)]
    interval = [float(i)/(n-1) for i in dist]
    colors = [tuple([int(x+y*i) for x, y in zip(hex2rgb(start), interval)])
              for i in range(n)]
    return [rgb2hex(i) for i in colors]


def gradient(anchors, n):
    """takes a list of hexcodes and number of colors to include and returns a list of colors in the gradient as hexcodes.

    Arguments:
        anchors {list} -- a list of hexcodes as strings with leading # included
        n {int} -- number of colors in the gradient

    Returns:
        list -- a list of color hexcodes as strings
    """
    if len(anchors) > 2:
        n += len(anchors)-2

    # okay if steps divisible by anchors
    steps = [int(n/(len(anchors)-1)) for i in range(len(anchors)-1)]
    '''last gradient gets all the extra steps'''
    if sum(steps) != n:
        steps.append(steps[-1]+n-sum(steps))
        del steps[-2]

    # calculates the gradient
    output = []
    for i in range(len(anchors)-1):
        start = anchors[i]
        stop = anchors[i+1]
        ns_colors = int(steps[i])
        c_i = bigradient(start, stop, ns_colors)
        for j in c_i:
            output.append(j)

    return list(OrderedDict.fromkeys(output))


def display_colors(colors):
    """Takes in a color of list of colors, saves and returns image of color swatches

    Arguments:
        colors {list} -- a list of hexcodes of rgb triplets

    Returns:
        image -- PIL image object
    """
    if type(colors) == str and colors.startswith('#'):  # input and be a single hexcode
        colors = [hex2rgb(colors)]
    # input can be a list of hexcodes
    elif type(colors) == list and type(colors[0]) == str and colors[0].startswith('#'):
        colors = [hex2rgb(i) for i in colors]
    # input can be a list of rgb triplets
    elif type(colors) == list and type(colors[0]) == list:
        colors = colors
    # input can be a single rgb triplet
    elif type(colors) == list and type(colors[0]) == int:
        colors = [colors]

    single_width = int(1000 / len(colors))
    im = [Image.new("RGB", (single_width, 100), j) for j in colors]

    # The width and height of the background tile
    im_w, im_h = im[0].size

    # Creates a new empty image, RGB mode, and size 1000 by 1000
    new_im = Image.new('RGB', (single_width*len(colors), 100))

    # The width and height of the new image
    w, h = new_im.size

    # Iterate through a grid, to place the background tile
    for i in range(0, len(colors)):
            # paste the image at location i, j:
        new_im.paste(im[i], (i*im_w, 0))
    new_im.save('colors.png')
    return new_im

###################################################
#########   Rarely usted table formats   #########
###################################################


def layout_heatmap_table(hm):

    hm['layout']['margin'] = {"t": 60,
                              "b": 70,
                              "l": 100,
                              "r": 100,
                              "pad": 0}
    hm['layout']['width'] = 300

    return hm


def format_table(table, df, seed=['white', '#eaeef4']):

    numb_rows = len(df.iloc[:, 0])
    factor = int(numb_rows/len(seed))
    row_color = sum([seed for i in range(0, factor)], []) + \
        seed[:numb_rows % len(seed)]

    table['header']['fill']['color'] = '#486ea0'
    table['header']['align'] = ['center'] * len(df.columns)
    table['header']['line'] = {'color': '#486ea0'}
    table['header']['font'] = dict(color='white', size=14,
                                   family='Helvetica, Arial, Gravitas One')
    table['cells']['fill']['color'] = [row_color]
    table['cells']['align'] = ['center'] * len(df.columns)
    table['cells']['line'] = {'color': [row_color]}
    table['cells']['font'] = dict(color='black', size=12,
                                  family='Helvetica, Arial, Gravitas One')
    return table


def format_indexed_table(table, df):
    basic_table = format_table(table, df)
    basic_table['cells']['fill']['color'] = ['lightgrey'] + \
        ['white' for i in range(len(df.columns)-1)]
    basic_table['cells']['line']['color'] = ['lightgrey'] + \
        ['white' for i in range(len(df.columns)-1)]
    return basic_table


def format_mutirxn_table(table, df, seed=['#f3f1fe', '#d8e3fd', '#bed5fc', '#a3c7fb', '#bed5fc', '#d8e3fd', '#f3f1fe']):
    table2 = format_table(table, df)
    reactions = df.iloc[:, 0].unique()
    factor = int(len(reactions)/len(seed))
    row_color = sum([seed for i in range(0, factor)], []) + \
        seed[:len(reactions) % len(seed)]
    # I have a list of colors for each unique index in order of appearance

    rxn_cls = dict((k, seed[list(reactions).index(k)]) for k in reactions)
    table2['cells']['fill']['color'] = [[rxn_cls[i] for i in df.iloc[:, 0]]]
    table2['cells']['line']['color'] = [[rxn_cls[i] for i in df.iloc[:, 0]]]
    return table2


if __name__ == '__main__':
    print(gradient([gruv_dark[1], gruv_mono[-1], gruv_dark[4]], 11))
    display_colors(gradient([gruv_dark[1], gruv_mono[-1], gruv_dark[4]], 11))
